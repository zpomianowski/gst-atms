#include "player.h"

static const char * Type2Name(int TypeNum)
{
    static char buf[32];
    if (TypeNum<200 || (TypeNum>=2000&&TypeNum<3000))
        sprintf(buf, "DTA-%d", TypeNum);
    else if (TypeNum>=200 && TypeNum <400)
        sprintf(buf, "DTU-%d", TypeNum);
    else if (TypeNum>3000)
        sprintf(buf, "DTE-%d", TypeNum);
    else
        sprintf(buf, "%d", TypeNum);
    return buf;
}

DTPlayer::DTPlayer(_GstDtapisink * p): parent(p), m_DvcType(215), m_DvcNo(0)
{

}

DTPlayer::~DTPlayer()
{

}

// properties
void
DTPlayer::setDvcType(int type)
{
    this->m_DvcType = type;
}

int
DTPlayer::getDvcType()
{
    return this->m_DvcType;
}

void
DTPlayer::setDvcNo(int no)
{
    this->m_DvcNo = no;
}

int
DTPlayer::getDvcNo()
{
    return this->m_DvcNo;
}

// control
void
DTPlayer::AttachToOutput()
{
    DTAPI_RESULT dr;

    dr = m_DtDvc.AttachToType(this->m_DvcType, this->m_DvcNo);
    if (dr != DTAPI_OK) {
        GST_ERROR_OBJECT (this->parent,
            c_ErrFailToAttachToType,
            Type2Name(this->m_DvcType), DtapiResult2Str(dr));
    }
}
