/* GStreamer
 * Copyright (C) 2016 Zbigniew Pomianowski <zpomianowski@cyfrowypolsat.pl>
	 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
	 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-dtapisink
 *
 * Element for streaming to DVB-T/DVB-C Dektec modulator.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
     * gst-launch -v -m fakesrc ! dtapisink
 * ]|
	 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include "gstdtapisink.h"
#include "../../DTAPI.h"

GST_DEBUG_CATEGORY_STATIC (gst_dtapisink_debug_category);
#define GST_CAT_DEFAULT gst_dtapisink_debug_category
#define BUFSIZE (512 * 1024)

/* prototypes */


static void gst_dtapisink_set_property (GObject * object,
                                        guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_dtapisink_get_property (GObject * object,
                                        guint property_id, GValue * value, GParamSpec * pspec);
static void gst_dtapisink_dispose (GObject * object);
static void gst_dtapisink_finalize (GObject * object);

static GstCaps *gst_dtapisink_get_caps (GstBaseSink * sink, GstCaps * filter);
static gboolean gst_dtapisink_set_caps (GstBaseSink * sink, GstCaps * caps);
static GstCaps *gst_dtapisink_fixate (GstBaseSink * sink, GstCaps * caps);
static gboolean gst_dtapisink_activate_pull (GstBaseSink * sink, gboolean active);
static void gst_dtapisink_get_times (GstBaseSink * sink, GstBuffer * buffer,
                                     GstClockTime * start, GstClockTime * end);
static gboolean gst_dtapisink_propose_allocation (GstBaseSink * sink,
                                                  GstQuery * query);
static gboolean gst_dtapisink_start (GstBaseSink * sink);
static gboolean gst_dtapisink_stop (GstBaseSink * sink);
static gboolean gst_dtapisink_unlock (GstBaseSink * sink);
static gboolean gst_dtapisink_unlock_stop (GstBaseSink * sink);
static gboolean gst_dtapisink_query (GstBaseSink * sink, GstQuery * query);
static gboolean gst_dtapisink_event (GstBaseSink * sink, GstEvent * event);
static GstFlowReturn gst_dtapisink_wait_event (GstBaseSink * sink,
                                               GstEvent * event);
static GstFlowReturn gst_dtapisink_prepare (GstBaseSink * sink,
                                            GstBuffer * buffer);
static GstFlowReturn gst_dtapisink_prepare_list (GstBaseSink * sink,
                                                 GstBufferList * buffer_list);
static GstFlowReturn gst_dtapisink_preroll (GstBaseSink * sink,
                                            GstBuffer * buffer);
static GstFlowReturn gst_dtapisink_render (GstBaseSink * sink,
                                           GstBuffer * buffer);
static GstFlowReturn gst_dtapisink_render_list (GstBaseSink * sink,
                                                GstBufferList * buffer_list);

enum
{
	PROP_0
};

/* pad templates */

static GstStaticPadTemplate gst_dtapisink_sink_template =
GST_STATIC_PAD_TEMPLATE (
    "sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (
    "video/mpegts, mpegversion = (int) 2, "
        "txmode = (string) { 188, ADD16, 192, 204, MIN16, RAW, RAWSI }"
        // "mt = (string) { ATSC, CMMB, DAB, DTMB, DVBS, DVBS2_16APSK, "
        //     "DVBS2_32APSK, DVBS2_8PSK, DVBS2_QPSK, DVBS2_L3, "
        //     "DVBT, ISDBS, ISDBT, IQ, QAM4, QAM16, QAM32, "
        //     "QAM64, QAM128, QAM256, T2MI }, "
        // "mf = (int) [ 1, 999999 ], " // FIXME
        // "ml = (int) [-1000, 1000]" //FIXME
));


/* class initialization */

G_DEFINE_TYPE_WITH_CODE (GstDtapisink, gst_dtapisink, GST_TYPE_BASE_SINK,
    GST_DEBUG_CATEGORY_INIT (
        gst_dtapisink_debug_category, "dtapisink",
        GST_DEBUG_BG_YELLOW,
        "debug category for dtapisink element"));

static void
gst_dtapisink_class_init (GstDtapisinkClass * klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GstBaseSinkClass *base_sink_class = GST_BASE_SINK_CLASS (klass);

	/* Setting up pads and setting metadata should be moved to
	 base_class_init if you intend to subclass this class. */
	gst_element_class_add_static_pad_template (GST_ELEMENT_CLASS(klass),
	                                           &gst_dtapisink_sink_template);

	gst_element_class_set_static_metadata (GST_ELEMENT_CLASS(klass),
	                                       "FIXME Long name", "Generic", "FIXME Description",
	                                       "FIXME <fixme@example.com>");

	// gobject_class->set_property = gst_dtapisink_set_property;
	// gobject_class->get_property = gst_dtapisink_get_property;
	gobject_class->dispose = gst_dtapisink_dispose;
	gobject_class->finalize = gst_dtapisink_finalize;
	// base_sink_class->get_caps = GST_DEBUG_FUNCPTR (gst_dtapisink_get_caps);
	// base_sink_class->set_caps = GST_DEBUG_FUNCPTR (gst_dtapisink_set_caps);
	// base_sink_class->fixate = GST_DEBUG_FUNCPTR (gst_dtapisink_fixate);
	// base_sink_class->activate_pull = GST_DEBUG_FUNCPTR (gst_dtapisink_activate_pull);
	// base_sink_class->get_times = GST_DEBUG_FUNCPTR (gst_dtapisink_get_times);
	// base_sink_class->propose_allocation = GST_DEBUG_FUNCPTR (gst_dtapisink_propose_allocation);
	// base_sink_class->start = GST_DEBUG_FUNCPTR (gst_dtapisink_start);
	// base_sink_class->stop = GST_DEBUG_FUNCPTR (gst_dtapisink_stop);
	// base_sink_class->unlock = GST_DEBUG_FUNCPTR (gst_dtapisink_unlock);
	// base_sink_class->unlock_stop = GST_DEBUG_FUNCPTR (gst_dtapisink_unlock_stop);
	// base_sink_class->query = GST_DEBUG_FUNCPTR (gst_dtapisink_query);
	base_sink_class->event = GST_DEBUG_FUNCPTR (gst_dtapisink_event);
	// base_sink_class->wait_event = GST_DEBUG_FUNCPTR (gst_dtapisink_wait_event);
	// base_sink_class->prepare = GST_DEBUG_FUNCPTR (gst_dtapisink_prepare);
	// base_sink_class->prepare_list = GST_DEBUG_FUNCPTR (gst_dtapisink_prepare_list);
	// base_sink_class->preroll = GST_DEBUG_FUNCPTR (gst_dtapisink_preroll);
	base_sink_class->render = GST_DEBUG_FUNCPTR (gst_dtapisink_render);
	// base_sink_class->render_list = GST_DEBUG_FUNCPTR (gst_dtapisink_render_list);
}

static void
gst_dtapisink_init (GstDtapisink *dtapisink)
{
    gst_base_sink_set_sync (GST_BASE_SINK (dtapisink), FALSE);
    gst_base_sink_set_blocksize (GST_BASE_SINK (dtapisink), BUFSIZE / 2);
    dtapisink->player = new DTPlayer(dtapisink);
    dtapisink->player->AttachToOutput();
}

void
gst_dtapisink_set_property (GObject * object, guint property_id,
                            const GValue * value, GParamSpec * pspec)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (object);

	GST_ERROR_OBJECT (dtapisink, "set_property");

	switch (property_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

void
gst_dtapisink_get_property (GObject * object, guint property_id,
                            GValue * value, GParamSpec * pspec)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (object);

	GST_ERROR_OBJECT (dtapisink, "get_property");

	switch (property_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

void
gst_dtapisink_dispose (GObject * object)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (object);

	GST_ERROR_OBJECT (dtapisink, "dispose");

	/* clean up as possible.  may be called multiple times */

	G_OBJECT_CLASS (gst_dtapisink_parent_class)->dispose (object);
}

void
gst_dtapisink_finalize (GObject * object)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (object);

	GST_ERROR_OBJECT (dtapisink, "finalize");

	/* clean up object here */

	G_OBJECT_CLASS (gst_dtapisink_parent_class)->finalize (object);
}

static GstCaps *
gst_dtapisink_get_caps (GstBaseSink * sink, GstCaps * filter)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "get_caps");

	return NULL;
}

/* notify subclass of new caps */
static gboolean
gst_dtapisink_set_caps (GstBaseSink * sink, GstCaps * caps)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "set_caps");
    //
    // GstStructure *structure;
    // GstCaps *incaps;
    // const gchar *txmode;
    // gboolean ret;
    //
    // structure = gst_caps_get_structure (caps, 0);
    // txmode = gst_structure_get_string (structure, "txmode");
    // // incaps = gst_caps_new_simple ("vide/mpegts",
    // //     "txmode", G_TYPE_STRING, txmode);
    // incaps = gst_caps_new_any ();
    //
    // ret = gst_pad_set_caps (GST_BASE_SINK (dtapisink)->sinkpad, incaps);
    // gst_caps_unref (incaps);
    return TRUE;
}

/* fixate sink caps during pull-mode negotiation */
static GstCaps *
gst_dtapisink_fixate (GstBaseSink * sink, GstCaps * caps)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "fixate");

	return NULL;
}

/* start or stop a pulling thread */
static gboolean
gst_dtapisink_activate_pull (GstBaseSink * sink, gboolean active)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "activate_pull");

	return TRUE;
}

/* get the start and end times for syncing on this buffer */
static void
gst_dtapisink_get_times (GstBaseSink * sink, GstBuffer * buffer,
                         GstClockTime * start, GstClockTime * end)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "get_times");

}

/* propose allocation parameters for upstream */
static gboolean
gst_dtapisink_propose_allocation (GstBaseSink * sink, GstQuery * query)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "propose_allocation");

	return TRUE;
}

/* start and stop processing, ideal for opening/closing the resource */
static gboolean
gst_dtapisink_start (GstBaseSink * sink)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "start");

	return TRUE;
}

static gboolean
gst_dtapisink_stop (GstBaseSink * sink)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "stop");

	return TRUE;
}

/* unlock any pending access to the resource. subclasses should unlock
 * any function ASAP. */
static gboolean
gst_dtapisink_unlock (GstBaseSink * sink)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "unlock");

	return TRUE;
}

/* Clear a previously indicated unlock request not that unlocking is
 * complete. Sub-classes should clear any command queue or indicator they
 * set during unlock */
static gboolean
gst_dtapisink_unlock_stop (GstBaseSink * sink)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "unlock_stop");

	return TRUE;
}

/* notify subclass of query */
static gboolean
gst_dtapisink_query (GstBaseSink * sink, GstQuery * query)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "query");

	return TRUE;
}

/* notify subclass of event */
static gboolean
gst_dtapisink_event (GstBaseSink * sink, GstEvent * event)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "event");

    gboolean ret;
    switch (GST_EVENT_TYPE (event)) {
        case GST_EVENT_CAPS:
        {
            GstCaps *caps;
            gst_event_parse_caps (event, &caps);
            ret = gst_dtapisink_set_caps (sink, caps);
            break;
        }
        default:
            ret = TRUE;
            break;
    }
    return ret;
}

/* wait for eos or gap, subclasses should chain up to parent first */
static GstFlowReturn
gst_dtapisink_wait_event (GstBaseSink * sink, GstEvent * event)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "wait_event");

	return GST_FLOW_OK;
}

/* notify subclass of buffer or list before doing sync */
static GstFlowReturn
gst_dtapisink_prepare (GstBaseSink * sink, GstBuffer * buffer)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "prepare");

	return GST_FLOW_OK;
}

static GstFlowReturn
gst_dtapisink_prepare_list (GstBaseSink * sink, GstBufferList * buffer_list)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "prepare_list");

	return GST_FLOW_OK;
}

/* notify subclass of preroll buffer or real buffer */
static GstFlowReturn
gst_dtapisink_preroll (GstBaseSink * sink, GstBuffer * buffer)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "preroll");

	return GST_FLOW_OK;
}

static GstFlowReturn
gst_dtapisink_render (GstBaseSink * sink, GstBuffer * buffer)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "dupa");

	return GST_FLOW_OK;
}

/* Render a BufferList */
static GstFlowReturn
gst_dtapisink_render_list (GstBaseSink * sink, GstBufferList * buffer_list)
{
	GstDtapisink *dtapisink = GST_DTAPISINK (sink);

	GST_ERROR_OBJECT (dtapisink, "render_list");

	return GST_FLOW_OK;
}

static gboolean
plugin_init (GstPlugin * plugin)
{
	return gst_element_register (plugin, "dtapisink", GST_RANK_NONE,
	                             GST_TYPE_DTAPISINK);
}

#ifndef VERSION
#define VERSION "0.0.1"
#endif
#ifndef PACKAGE
#define PACKAGE "DTAPI"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "DTAPI package"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "http://FIXME.org/"
#endif

GST_PLUGIN_DEFINE (
	GST_VERSION_MAJOR,
	GST_VERSION_MINOR,
	dtapisink,
	"Sink element for general-purpose Dektec test modulator in your R&D lab \
	for developing, qualifying or repairing any \
	equipment with an antenna input",
	plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)
