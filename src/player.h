#ifndef __DTPLAYER_H
#define __DTPLAYER_H

#include <gst/gst.h>
#include <stdio.h>
#include <stdarg.h>
#include "../../DTAPI.h"

typedef struct _GstDtapisink GstDtapisink;

// General errors
const char c_ErrDtapiDvcScanFailed[]    = "DtapiDeviceScan failed (ERROR: %s)";
const char c_ErrNoOutputFound[]         = "No output device in the system";
const char c_ErrNoSuchPort[]            = "The %s has no port %d";
const char c_ErrHwScanFailed[]          = "HwFuncScan failed (ERROR: %s)";
const char c_ErrNoOutputPort[]          = "The %s port %d it not an output port";
const char c_ErrNoSuitablePort[]        = "No suitable port found on the %s";
const char c_ErrFailToAttachToChan[]    = "Can't attach to the channel (ERROR: %s)";
const char c_ErrFailsafeEnabled[]       = "DtPlay doesn't support failsafe but it is enabled. Use DtInfo to disable failsafe first.";
const char c_ErrFailToAttachToType[]    = "Failed to attach to the %s (ERROR: %s)";
const char c_ErrDvcNumWithoutType[]     = "Specifying a device number without a type is not supported";
const char c_ErrDblbufSelf[]            = "Can't enabled buffered output on the port itself";
const char c_ErrFailGetFlags[]          = "GetFlags failed (ERROR: %s)";
const char c_ErrFailToSetIoConfig[]     = "Failed to set IO-configuration (ERROR: %s)";
const char c_ErrFailToGetIoConfig[]     = "Failed to get IO-configuration (ERROR: %s)";
const char c_ErrFailToGetIoStd[]        = "DtapiVidStd2IoStd failed (ERROR: %s)";
const char c_ErrFailToGetVidStdInfo[]   = "DtapiGetVidStdInfo failed (ERROR: %s)";
const char c_ErrFailToOpenFile[]        = "Can't open '%ls' for reading";
const char c_ErrReadFile[]              = "File read error";
const char c_ErrFailSetTxControl[]      = "SetTxControl failed (ERROR: %s)";
const char c_ErrFailSetTxMode[]         = "SetTxMode failed (ERROR: %s)";
const char c_ErrFailSetModControl[]     = "SetModControl failed (ERROR: %s)";
const char c_ErrFailSetRfControl[]      = "SetRfControl failed (ERROR: %s)";
const char c_ErrFailSetIpPars[]         = "SetIpPars failed (ERROR: %s)";
const char c_ErrFailSetTsRate[]         = "SetTsrateBps failed (ERROR: %s)";
const char c_ErrFailWrite[]             = "Write failed (ERROR: %s)";
const char c_ErrInvalidFileSize[]       = "Invalid file size";
const char c_ErrInvalidDtSdiFileHdr[]   = "Invalid DTSDI file header";
const char c_ErrFailedToSetOutputLevel[]= "Failed to set output level (ERROR: %s)";
const char c_ErrFailedToSetSNR[]        = "Failed to set SNR (ERROR: %s)";
const char c_FailedToInitIdsbtPars[]    = "Failed to initialise ISDB-T parameters (ERROR: %s)";
const char c_ErrFailGetFifoSize[]       = "Failed to get Fifo size (ERROR: %s)";
const char c_ErrCmmbTsRateFromTs[]      = "Cannot retrieve rate from stream (ERROR: %s)";
const char c_ErrCpuUnderflow[]          = "CPU underflow detected";
const char c_ErrDmaUnderflow[]          = "Dma underflow detected";
const char c_ErrFifoUnderflow[]         = "Fifo underflow detected";

class DTPlayer
{
public:
    DTPlayer(_GstDtapisink *);
    virtual ~DTPlayer();

    int PushData(GstBuffer * p);

    // properties
    void setDvcType(int);
    int getDvcType();

    void setDvcNo(int);
    int getDvcNo();

    // control
    void AttachToOutput();

private:
    _GstDtapisink * parent;
    int m_DvcType;
    int m_DvcNo;
    DtDevice  m_DtDvc;
    DtOutpChannel  m_DtOutp;
};

#endif // #ifndef __DTPLAYER_H
