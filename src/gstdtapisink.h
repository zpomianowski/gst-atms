/* GStreamer
 * Copyright (C) 2016 Zbigniew Pomianowski <zpomianowski@cyfrowypolsat.pl>
	 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
	 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_DTAPISINK_H_
#define _GST_DTAPISINK_H_

#include <gst/base/gstbasesink.h>
#include "player.h"

G_BEGIN_DECLS

#define GST_TYPE_DTAPISINK   (gst_dtapisink_get_type())
#define GST_DTAPISINK(obj)   (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_DTAPISINK,GstDtapisink))
#define GST_DTAPISINK_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_DTAPISINK,GstDtapisinkClass))
#define GST_IS_DTAPISINK(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_DTAPISINK))
#define GST_IS_DTAPISINK_CLASS(obj)   (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_DTAPISINK))

typedef struct _GstDtapisink GstDtapisink;
typedef struct _GstDtapisinkClass GstDtapisinkClass;

struct _GstDtapisink
{
	GstBaseSink base_dtapisink;
	DTPlayer * player;
};

struct _GstDtapisinkClass
{
	GstBaseSinkClass base_dtapisink_class;
};

GType gst_dtapisink_get_type (void);

G_END_DECLS

#endif
