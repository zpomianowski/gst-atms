# gst-dektec
Gstreamer-1.0 plugin with dtapisink element. It uses DTAPI to handle DVB-X USB modulators.

## Dependencies
You need gstreamer-1.0 dev packages and Dektec SDK with proper USB DVB modulator.
Please fix the path in `gstdtapisink.cpp` to the `DTAPI.h` file.

## How to build
You need autotools (autoscan, autoconf, autoheader, automake, libtoolize and maybe other shit).
```bash
autoscan
libtoolize
aclocal
autoheader
autoconf
automake --add-missing
./configure
```
..or open the Anjuta project - it should be able to handle the build process.

## Installation
```bash
make
sudo make install
```
For Ubuntu 14-16 machines the installation path should be: `/usr/lib/x86_64-linux-gnu/gstreamer-1.0`

## Test run
```bash
gst-launch-1.0 filesrc location=< MPEGTS_FILE_PATH > ! dtapisink
```

or more verbose:

```bash
gst-launch-1.0 --gst-debug-level=2 filesrc location=< MPEGTS_FILE_PATH > ! dtapisink
```

## How to build next plugin
Best way is to download [gst-plugins-bad](https://github.com/GStreamer/gst-plugins-bad.git) repo, and use `tools/gst-element-maker` script to genereate a boilerplate (in `tools/element-templates` many base elements you can derivate from).
Duplicate `gst-dektec` folder, rename, then copy `*.c` and `*.h` files into `src` folder and fix `Makefile.am` and maybe `configure.ac`.


